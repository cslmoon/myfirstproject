# My Job application history at GitLab

1. **Technical Marketing Manager, APAC** (15 Aug, 2019) - Unsuccessful (17 Sep, 2019)
2. **Senior Channel Sales Manager, APAC** (16 Aug, 2019) - Unsuccessful
3. **Channel Sales Manager, APAC** (16 Aug, 2019) - Unsuccessful
4. **[Technical Account Manager (Korea, Mid Market)](https://about.gitlab.com/jobs/apply/technical-account-manager-mid-market-south-korea-4340209002/)** (3 Oct)
    - Changed to a **Manager, Customer Experience (APAC)** (4 Nov, 2019) 
5. **Manager, Customer Experience (APAC)** (4 Nov, 2019) - Unsuccessful (2 Dec, 2019)
    - 1st Interview (8 Nov, 2019) with Kristen Lawrence.
    - All good. But she indicated that my last management experience was 10 years ago.
    - She told me that I could move forward after an interview with APAC Sales Head (Anthony) - It was not organised and somebody took that position.
6. **[Senior Channel Sales Manager (APAC)](https://about.gitlab.com/jobs/apply/senior-channel-sales-manager-apac-4474059002/)** (16 Oct, 2019) - no progress yet.
7. **[Open Source Program Manager](https://about.gitlab.com/jobs/apply/open-source-program-manager-4322397002/)** (26 Nov, 2019) - no progress yet
8. **[Area Sales Manager (SMB/APAC)](https://about.gitlab.com/jobs/apply/area-sales-manager-smb-apac-4316523002/) & [Mid Market Account Executive (APAC)](https://about.gitlab.com/jobs/apply/area-sales-manager-midmarket-apac-4312175002/)** (28 Nov, 2019) 
    - Interview with a recruiter (Debbie) on 16 Dec, 2019. - Alternatively, considered as a Strategic Account Leader because I didn't manage the people recenlty.
9. **[Community Advocate, APAC](https://boards.greenhouse.io/gitlab/jobs/4392296002)** (2 Dec, 2019) - Unsuccessful (15 Dec, 2019)
10. **[Strategic Account Leader, SEA](https://about.gitlab.com/jobs/apply/strategic-account-leader-sea-4584901002/)** (14 Jan, 2020) - in progress.
    - 1st Interview with a hiring manager (4 Mar, 2020) - Successful
    - 2nd Interview with a peer (6 Mar, 2020) - Successful
    - Final Interview (12 Mar, 2020) - Postponsed 😂😂😂